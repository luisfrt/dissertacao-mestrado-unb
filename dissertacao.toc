\select@language {american}
\select@language {american}
\select@language {brazil}
\contentsline {chapter}{\numberline {1}Introdu\IeC {\c c}\IeC {\~a}o}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Problema}{4}{section.1.1}
\contentsline {section}{\numberline {1.2}Principais Contribui\IeC {\c c}\IeC {\~o}es deste Trabalho}{5}{section.1.2}
\contentsline {section}{\numberline {1.3}Contribui\IeC {\c c}\IeC {\~o}es Espec\IeC {\'\i }ficas}{5}{section.1.3}
\contentsline {section}{\numberline {1.4}Organiza\IeC {\c c}\IeC {\~a}o do Texto}{6}{section.1.4}
\contentsline {chapter}{\numberline {2}Referencial Te\IeC {\'o}rico}{7}{chapter.2}
\contentsline {section}{\numberline {2.1}An\IeC {\'a}lise de Imagens em Patologia}{7}{section.2.1}
\contentsline {section}{\numberline {2.2}Casos de Uso}{9}{section.2.2}
\contentsline {section}{\numberline {2.3}Region Templates}{12}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Estruturas de Dados Otimizadas}{14}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Plataforma de Execu\IeC {\c c}\IeC {\~a}o Distribu\IeC {\'\i }da}{15}{subsection.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}Gerenciamento do Armazenamento de Dados e Otimiza\IeC {\c c}\IeC {\~o}es}{19}{subsection.2.3.3}
\contentsline {section}{\numberline {2.4}Otimiza\IeC {\c c}\IeC {\~a}o Multiobjetivo de Par\IeC {\^a}metros}{19}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}Domin\IeC {\^a}ncia, Otimalidade e Frente de Pareto}{20}{subsection.2.4.1}
\contentsline {subsection}{\numberline {2.4.2}Abordagens Para Resolu\IeC {\c c}\IeC {\~a}o de Problemas Multiobjetivo}{22}{subsection.2.4.2}
\contentsline {subsection}{\numberline {2.4.3}Escalariza\IeC {\c c}\IeC {\~a}o}{23}{subsection.2.4.3}
\contentsline {subsection}{\numberline {2.4.4}Teorema da Inexist\IeC {\^e}ncia de Almo\IeC {\c c}o Gr\IeC {\'a}tis}{23}{subsection.2.4.4}
\contentsline {section}{\numberline {2.5}Algoritmos Gen\IeC {\'e}ticos}{24}{section.2.5}
\contentsline {paragraph}{Inicializa\IeC {\c c}\IeC {\~a}o da Popula\IeC {\c c}\IeC {\~a}o}{27}{section*.22}
\contentsline {paragraph}{Avalia\IeC {\c c}\IeC {\~a}o}{27}{section*.23}
\contentsline {paragraph}{Sele\IeC {\c c}\IeC {\~a}o dos Reprodutores}{27}{section*.24}
\contentsline {paragraph}{Cruzamento (\emph {Crossover})}{28}{section*.25}
\contentsline {paragraph}{Muta\IeC {\c c}\IeC {\~a}o}{29}{section*.28}
\contentsline {paragraph}{Substitui\IeC {\c c}\IeC {\~a}o}{29}{section*.29}
\contentsline {paragraph}{Arrefecimento Simulado (\emph {Simulated Annealing})}{30}{section*.30}
\contentsline {paragraph}{Algoritmos Mem\IeC {\'e}ticos (\emph {Memetic Algorithms})}{30}{section*.31}
\contentsline {section}{\numberline {2.6}Ajuste de Par\IeC {\^a}metros em Aplica\IeC {\c c}\IeC {\~o}es de Segmenta\IeC {\c c}\IeC {\~a}o}{30}{section.2.6}
\contentsline {chapter}{\numberline {3}Ajuste Autom\IeC {\'a}tico de Par\IeC {\^a}metros}{33}{chapter.3}
\contentsline {section}{\numberline {3.1}Sistema de Ajuste Autom\IeC {\'a}tico de Par\IeC {\^a}metros}{34}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Estrat\IeC {\'e}gia de Generaliza\IeC {\c c}\IeC {\~a}o da Otimiza\IeC {\c c}\IeC {\~a}o}{38}{subsection.3.1.1}
\contentsline {section}{\numberline {3.2}Algoritmos de Otimiza\IeC {\c c}\IeC {\~a}o Suportados}{38}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}M\IeC {\'e}todo Nelder-Mead (NM)}{39}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}M\IeC {\'e}todo Parallel Rank Order (PRO)}{39}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}M\IeC {\'e}todo de Otimiza\IeC {\c c}\IeC {\~a}o Bayesiana (Spearmint)}{40}{subsection.3.2.3}
\contentsline {subsection}{\numberline {3.2.4}Algoritmo Gen\IeC {\'e}tico Implementado (GA)}{41}{subsection.3.2.4}
\contentsline {paragraph}{Inicializa\IeC {\c c}\IeC {\~a}o da Popula\IeC {\c c}\IeC {\~a}o}{42}{section*.40}
\contentsline {paragraph}{Cruzamento}{43}{section*.45}
\contentsline {paragraph}{Muta\IeC {\c c}\IeC {\~a}o}{44}{section*.46}
\contentsline {paragraph}{Avalia\IeC {\c c}\IeC {\~a}o}{44}{section*.47}
\contentsline {paragraph}{Substitui\IeC {\c c}\IeC {\~a}o}{44}{section*.48}
\contentsline {paragraph}{Propaga\IeC {\c c}\IeC {\~a}o de Membros da Elite (Est\IeC {\'a}gio Opcional)}{44}{section*.49}
\contentsline {paragraph}{Sele\IeC {\c c}\IeC {\~a}o dos Reprodutores}{45}{section*.51}
\contentsline {paragraph}{Paralelismo}{46}{section*.52}
\contentsline {section}{\numberline {3.3}An\IeC {\'a}lises Comparativas}{46}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Consultas Espaciais (\emph {Spatial Queries})}{47}{subsection.3.3.1}
\contentsline {paragraph}{Biblioteca de An\IeC {\'a}lise Espacial: Hadoop-GIS}{47}{section*.54}
\contentsline {subsection}{\numberline {3.3.2}M\IeC {\'e}tricas Implementadas}{51}{subsection.3.3.2}
\contentsline {paragraph}{M\IeC {\'e}trica: \emph {Diff Pixels}}{51}{section*.57}
\contentsline {paragraph}{M\IeC {\'e}trica: \emph {General Dice Coefficient}}{51}{section*.58}
\contentsline {paragraph}{M\IeC {\'e}trica: \emph {Individual Dice Coefficient}}{51}{section*.59}
\contentsline {paragraph}{M\IeC {\'e}trica: \emph {Average Dice Coefficient}}{52}{section*.60}
\contentsline {paragraph}{M\IeC {\'e}trica: \emph {Intersection Overlap Area}}{53}{section*.62}
\contentsline {paragraph}{M\IeC {\'e}trica: \emph {Jaccard Index}}{53}{section*.63}
\contentsline {section}{\numberline {3.4}Ajuste Multiobjetivo}{54}{section.3.4}
\contentsline {chapter}{\numberline {4}Avalia\IeC {\c c}\IeC {\~a}o Experimental}{60}{chapter.4}
\contentsline {section}{\numberline {4.1}Configura\IeC {\c c}\IeC {\~a}o e Par\IeC {\^a}metros do GA}{61}{section.4.1}
\contentsline {paragraph}{Tamanho da Popula\IeC {\c c}\IeC {\~a}o e Quantidade de Gera\IeC {\c c}\IeC {\~o}es}{62}{section*.69}
\contentsline {paragraph}{Taxa de Muta\IeC {\c c}\IeC {\~a}o}{62}{section*.71}
\contentsline {paragraph}{Taxa de \emph {Crossover}}{62}{section*.73}
\contentsline {paragraph}{Taxa de Elitismo}{63}{section*.75}
\contentsline {section}{\numberline {4.2}Otimiza\IeC {\c c}\IeC {\~a}o de Cada Imagem Individualmente}{65}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Otimiza\IeC {\c c}\IeC {\~a}o Mono-Objetivo}{65}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Otimiza\IeC {\c c}\IeC {\~a}o Multiobjetivo}{67}{subsection.4.2.2}
\contentsline {section}{\numberline {4.3}Valida\IeC {\c c}\IeC {\~a}o Cruzada}{69}{section.4.3}
\contentsline {section}{\numberline {4.4}Valida\IeC {\c c}\IeC {\~a}o Cruzada em Dois Grupos}{70}{section.4.4}
\contentsline {chapter}{\numberline {5}Conclus\IeC {\~a}o}{75}{chapter.5}
\contentsline {chapter}{Refer\^encias}{79}{section*.83}
