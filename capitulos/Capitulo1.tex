\chapter{Introdução}%

O processamento de imagens em Patologia é uma das áreas da Ciência da Computação que tem proporcionado inúmeros avanços científicos em conjunto com a Medicina. A análise de imagens digitais em alta resolução de slides de tecidos (WSI\footnote{WSI - Whole Slide Imaging (Imagens digitais de slides de tecidos).}) permite o estudo de doenças em níveis celulares e sub-celulares  \cite{cooper2012integrated}. Esses estudos são realizados utilizando-se imagens digitais de microscopia médica em vez de lâminas físicas. Muitas doenças, como o câncer por exemplo, manifestam-se por meio de alterações na morfologia das células em escala micro-anatômica. Investigar essas mudanças, quantificar os seus efeitos e efetuar correlações com dados moleculares e resultados clínicos podem levar a uma melhor compreensão dos mecanismos dessas doenças, e permitir o desenvolvimento de novas formas de tratamento. Um exemplo de alteração causada por tumores cerebrais é a mudança na forma e textura dos núcleos das células comprometidas. Nesse caso, as células comprometidas passam, por exemplo, a se tornar mais alongadas, além de sofrerem alterações em sua textura \cite{cooper2012integrated,teodoro2015application}. 

Os instrumentos de captura de imagens médicas progrediram significativamente nos últimos anos, de maneira que atualmente eles podem capturar, a partir de uma amostra de tecido, imagens de resolução de até 120K x 120K \emph{pixels} em um \emph{scanner} estado da arte \cite{teodoro2014region}. Esses instrumentos são capazes de realizar a digitalização de amostras de tecidos humanos em poucos minutos \cite{kong2013machine}. Essas imagens sem compressão podem atingir aproximadamente 50 GB de tamanho.


%Neste banco é possível encontrar imagens de tecidos de variados tipos de cânceres %Uma WSI deste banco de dados pode exceder 2 GB de tamanho \cite{kong2011silico}. 




 Além disso, esses dispositivos de digitalização de tecidos estão se tornando cada vez mais populares nas unidades médicas devido à queda nos preços. Em decorrência da popularização dessas tecnologias, houve um crescimento significativo no tamanho das bases de dados de WSIs e, como consequência, existe uma grande demanda por tarefas eficientes relacionadas à compressão, armazenamento, visualização e análise desses dados. 
 
Existem aplicações de bioinformática capazes de processarem essas imagens WSI e
extraírem informações a respeito do estado de saúde de um paciente por meio da
análise das estruturas encontradas nessas imagens, como por exemplo, o tamanho
e o formato do núcleo das células presentes na amostra de tecido estudada
\cite{kong2013machine, veta2013automatic, coelho2009nuclear,stegmaier2016reali,liang20153d,liang2015liver}. No entanto, essas
aplicações são parametrizadas, e os valores desses parâmetros influenciam
significativamente os resultados da análise. Encontrar o ajuste ideal dessas
aplicações, ou seja, uma combinação de parâmetros de entrada que maximize a
qualidade do resultado é uma tarefa complexa que envolve uma grande quantidade
de combinações parâmetros a serem testados e de dados a serem processados.
Normalmente essas aplicações vêm pré-configuradas com uma combinação de
parâmetros que não é adequada para todos os tipos de imagens a serem
processadas pela aplicação \cite{teodoro2017algorithm}.

Para realizar esse ajuste de parâmetros, foi desenvolvido neste trabalho um sistema para otimizar a precisão e/ou minimizar o tempo de execução dessas aplicações de bioinformática. Para isso, foi desenvolvido um mecanismo de otimização multiobjetivo capaz de encontrar combinações de parâmetros melhores do que a configuração padrão oferecida por essas aplicações. Além disso, foram desenvolvidas múltiplas métricas para quantificar as alterações na morfologia das células e tecidos presentes nas imagens processadas, a fim de permitir análises mais precisas e eficientes.  
 
Foram utilizadas duas aplicações de bioinformática como caso de uso para este trabalho. Essas aplicações são parametrizadas e consistem em fluxos de operações e transformações que se dividem nos estágios de normalização, segmentação e extração de características.  O primeiro estágio é o estágio da normalização que é responsável por corrigir e normalizar as cores da imagem que será processada. O segundo estágio é o da segmentação no qual ocorre a extração dos objetos e estruturas (ex.: núcleo das células) das imagens. O terceiro e último estágio, extração de características, dessas aplicações realiza a computação das características desses objetos, como por exemplo o perímetro e a forma predominante das estruturas, entre outras características. O estágio de segmentação, principal alvo deste trabalho, possui três funções principais: i) identificar os núcleos das células presentes nas imagens de tecidos, ii) extraí-los e iii) produzir uma máscara como saída. Uma máscara, por exemplo, pode ser uma imagem que atribui a cor branca aos objetos (células) identificados, e preto para o fundo (Figura \ref{fig:wsi6-ref}). Após a identificação dessas células, são realizadas uma série de análises a respeito da sua morfologia, comprimento, área, formato, a fim de se extrair características das mesmas. 

Ambas aplicações são executadas sobre a plataforma de execução distribuída Region Templates \cite{teodoro2014region}. A primeira aplicação possui 15 parâmetros de entrada que podem assumir múltiplos valores, de maneira que o espaço de busca total seja de  21,4 trilhões de pontos ao se considerar todas as combinações de valores de parâmetros possíveis. Já a segunda aplicação utiliza 7 parâmetros de entrada e possui um espaço de busca de 2,8 bilhões combinações de parâmetros possíveis. Além disso, cada execução dessas aplicações pode levar várias horas de processamento em uma estação de trabalho comum quando imagens em alta resolução são utilizadas. 

%Devido ao grande tamanho das imagens, das estruturas de dados e dos resultados intermediários calculados durante o processo de análise dessas imagens de tecido, pode-se exceder a memória principal disponível das máquinas responsáveis pela análise. Por estas razões, é comum particionar a imagem WSI em regiões não sobrepostas para permitir a análise paralela.

%A plataforma Region Templates \cite{teodoro2014region, teodoro2015application}, é capaz de executar aplicações de bioinformática que tratam essa imensa quantidade de imagens médicas de maneira eficiente e distribuída. 
O estágio de segmentação dessas aplicações de bioinformática é responsável por identificar e extrair as células presentes nas amostras de tecidos. É essencial que essa identificação seja precisa, de maneira que seja encontrado o maior número de células possível e que a extração delas seja realizada da maneira fidedigna. O grau de precisão da segmentação é afetado pelos parâmetros de entrada da aplicação. Dessa forma, é muito importante que essas aplicações sejam configuradas com combinações de parâmetros que produzam resultados com alta acurácia. Dado o tamanho do espaço de busca e o alto custo associado ao testar cada combinação de parâmetros, faz-se necessária uma estratégia de otimização para encontrar boas combinações utilizando-se uma quantidade pequena de testes, uma vez que a busca exaustiva é inviável. Deste modo, foi proposto neste trabalho um sistema de ajuste automático de parâmetros, com suporte a múltiplos algoritmos de otimização, para otimizar a escolha dos valores dos parâmetros de aplicações de bioinformática responsáveis pela segmentação nuclear em imagens WSI. O ajuste automático de parâmetros tem como objetivo melhorar a precisão e diminuir o tempo de execução dos algoritmos de segmentação (otimização multiobjetivo) das duas aplicações de bioinformática utilizadas como caso de uso neste trabalho. 


 %Os estágios mais comuns presentes nessas aplicações de bioinformática consistem na normalização, segmentação e classificação das características (\emph{features}) extraídas das imagens. 
%Medir, quantificar e classificar as alterações na morfologia das células e tecidos estudados envolve o processamento de consultas espaciais (\emph{spatial queries}). Essas consultas servem para efetuar o cálculo da área de intersecção entre objetos (células, veias e etc), calcular a proximidade espacial entre eles, descobrir padrões espaciais globais, entre outras finalidades. 

Para medir a qualidade das segmentações geradas, nós introduzimos um módulo de análises comparativas à plataforma Region Templates, a fim de oferecer suporte a várias métricas de avaliação qualitativa para os múltiplos algoritmos de otimização suportados. O objetivo é que as aplicações exemplo sejam otimizadas a fim de gerarem máscaras o mais semelhantes possível à máscara de referência anotada manualmente pelo patologista, conforme exemplo na Figura \ref{fig:wsi6-ref}. Uma vez ajustada, as aplicações exemplo serão capazes de reproduzir essa tarefa de identificação de células de maneira precisa e escalável.

%Nessa integração, o Region Templates é responsável por executar e escalonar os diversos estágios de uma aplicação biomédica em ambientes distribuídos de maneira eficiente e escalável, além de pré-processar as consultas espaciais antes de utilizar uma biblioteca para refinar o processamento espacial.

%Este módulo é capaz de realizar consultas espaciais de maneira dinâmica (\emph{on-the-fly}) e eficiente para comparar as máscaras produzidas pelos algoritmos de segmentação das aplicações de bioinformática. 

%Foi utilizada uma ferramenta chamada Hadoop-GIS \cite{aji2013hadoop} para efetuar parte do processamento das consultas espaciais. Esta ferramenta por si só não é capaz de executar consultas espaciais de maneira dinâmica (\emph{on-the-fly}), ou seja, ela não é capaz de processar uma consulta espacial e retornar os resultados a partir de uma chamada de outra aplicação. O núcleo (\emph{engine}) dessa ferramenta foi integrado como uma biblioteca do módulo de análises comparativas do Region Templates através de uma interface desenvolvida para conectar os dois \emph{frameworks}. Através dessa integração foi possível realizar consultas espaciais de maneira dinâmica (\emph{on-the-fly}) e eficiente para comparar as máscaras produzidas pelos algoritmos de segmentação das aplicações biomédicas. Nessa integração, o Region Templates é responsável por executar e escalonar os diversos estágios de uma aplicação biomédica em ambientes distribuídos de maneira eficiente e escalável, além de pré-processar as consultas espaciais antes de utilizar o Hadoop-GIS para refinar o processamento espacial.





\begin{figure}
\centering
\begin{subfigure}[t]{.30\linewidth}
  	\centering
	\includegraphics[width=0.9\textwidth]{imagens/image06-in} 
	\caption{Exemplo de imagem de tecido humano utilizada como entrada para o estágio de segmentação das aplicações exemplo \cite{images15}.} 
	\label{fig:wsi6-in} 
\end{subfigure}%
\quad
\begin{subfigure}[t]{.30\linewidth}
  	\centering
	\includegraphics[width=0.9\textwidth]{imagens/image06-intermediate} 
	\caption{Anotações manuais do patologista especialista na imagem (a). As partes circuladas são as células identificadas por ele. Elas serão pintadas de branco na máscara de referência (c) \cite{images15}.\\} 
	\label{fig:wsi6-intermediate} 
\end{subfigure}%
 \quad
\begin{subfigure}[t]{.30\linewidth}
  	\centering
	\includegraphics[width=0.9\textwidth]{imagens/image06-ref} 
	\caption{Exemplo de máscara gerada a partir das anotações manuais do patologista. A saída do estágio de segmentação das aplicações exemplo produzem máscaras semelhantes a esta \cite{images15}. } 
	\label{fig:wsi6-ref} 
\end{subfigure}%
 \quad
\caption{Exemplo de uma imagem de tecido humano que pode ser analisada pelas aplicações de bioinformática utilizadas como exemplo neste trabalho.}
\label{fig:in-intermediate-out}
\end{figure}

\FloatBarrier



%Nesta tarefa de otimização, é feita uma pesquisa inteligente no espaço de parâmetros de cada aplicação, de acordo com os parâmetros sugeridos pelos algoritmos de otimização.

%o espaço de parâmetros das aplicações exemplo é pesquisado de acordo com os parâmetros sugeridos pelos algoritmos de otimização, comparando-se os resultados dos estágios de segmentação dessas aplicações com a máscara de referência. 

%Esse ajuste serve para encontrar um conjunto de parâmetros que melhor configure as aplicações exemplo a fim de que elas produzam resultados que sejam mais similares à máscara de referência (de acordo com as métricas de comparação suportadas pelo nosso sistema) e/ou que minimizem o tempo de execução dessas aplicações sem testar todas as combinações possíveis de maneira exaustiva. 

O sistema de ajuste automático proposto nesta dissertação possui atualmente suporte para quatro algoritmos de otimização, sendo que o primeiro deles foi implementado inteiramente neste trabalho e os outros três foram implementados com o auxílio de bibliotecas elaboradas por terceiros. O algoritmo de otimização desenvolvido neste trabalho é um algoritmo genético (GA) projetado para imitar os princípios evolutivos da seleção natural \cite{deb1998genetic, michalewicz2013genetic}. Cada indivíduo da população GA foi modelado para representar um conjunto de parâmetros da aplicação, sendo assim uma potencial solução para o problema. %Cada parâmetro corresponde a um gene de um indivíduo. Ao todo, os 15 ou os 7 parâmetros, dependendo da aplicação, são modelados em genes em cada um dos indivíduos da população do algoritmo. Em seguida, usamos a reprodução e a mutação para evoluir os membros da população, a fim de obter valores de parâmetros que produzam resultados melhores. Este método de busca e optimização heurística é inerentemente paralelo porque cada indivíduo é avaliado independentemente \cite{sareni1998fitness}.  O algoritmo pode melhorar continuamente a aptidão da população até um número programado de gerações (iterações) ser atingido ou até que um critério de convergência seja alcançado. 
Os outros três algoritmos de otimização suportados são o Nelder-Mead (NM)\cite{Nelder01011965}, o Parallel Rank Order (PRO) \cite{tiwari2011online} e Otimização Bayesiana (Spearmint) \cite{snoek2012practical}. Eles serão descritos em detalhes no Capítulo \ref{chap:auto-tuning}.

\section{Problema}
Aplicações de processamento de imagens são inerentemente parametrizadas e os
valores desses parâmetros podem afetar os resultados significativamente. Ajustar
esses parâmetros conforme a métrica de interesse é complexo devido à grande quantidade de combinações parâmetros a serem processados.

\section{Principais Contribuições deste Trabalho}
A principal contribuição dessa dissertação é o desenvolvimento de uma solução eficiente e eficaz capaz de otimizar a precisão e/ou minimizar o tempo de execução do estágio de segmentação nuclear de aplicações médicas. Para isso foi desenvolvido um sistema de otimização multiobjetivo que ajusta automaticamente os parâmetros dessas aplicações. Além disso, utilizou-se múltiplas métricas e mecanismos de consultas espaciais para quantificar as alterações na morfologia das células e tecidos em escala micro-anatômica a fim de realizar análises mais precisas e eficientes.
\section{Contribuições Específicas}
\begin{itemize}
	
	\item Foi proposto e desenvolvido um sistema de otimização multiobjetivo com suporte a múltiplos algoritmos de otimização para ajuste de parâmetros de aplicações de bioinformática. Além disso, esse sistema de otimização multiobjetivo foi integrado ao ambiente de execução Region Templates como um módulo adicional para permitir que ele fosse utilizado em outros casos de uso.
		\item Foram desenvolvidas múltiplas métricas baseadas em consultas comparativas espaciais que são capazes de quantificar as alterações na morfologia das células e tecidos em escala micro-anatômica. Essas métricas e consultas espaciais foram adicionadas à plataforma Region Templates de forma que possam ser integradas à diferentes aplicações no futuro;
	\item Entre os algoritmos de otimização suportados, desenvolveu-se integralmente um algoritmo genético (GA) capaz de otimizar a qualidade e o tempo de execução.  Foram executados múltiplos testes comparativos de precisão entre o GA e os outros algoritmos de otimização suportados pelo Region Templates. Este algoritmo mostrou desempenho igual ou superior, na maioria dos casos, em relação aos outros algoritmos de otimização suportados;
	%\item Propor e implementar técnicas de otimização na velocidade de convergência do algoritmo genético;
		\item Otimizou-se a qualidade e reduziu-se o tempo de execução das aplicações de segmentação nuclear de tecidos utilizadas como caso de uso neste trabalho.
	%\item Generalizar as consultas espaciais para lidarem com o particionamento da imagem de entrada entre os múltiplos nós de processamento; e
	%\item Paralelizar a computação das consultas espaciais utilizando o coprocessador Intel\textsuperscript{\textregistered} Xeon Phi \textsuperscript{\texttrademark}.

\end{itemize}

Algumas dessas contribuições já foram publicadas no periódico \emph{Bioinformatics} \cite{teodoro2017algorithm}. Entre elas estão o sistema de ajuste automático de parâmetros mono-objetivo, quatro das seis métricas atualmente suportadas pelo sistema e o algoritmo genético. O restante das contribuições, como por exemplo, o sistema de ajuste automático multiobjetivo, as novas métricas e os resultados experimentais, parte mais recente do trabalho, serão publicadas em um outro artigo que está em elaboração.


\section{Organização do Texto}


O restante deste trabalho está organizado da seguinte forma. No Capítulo 2, Referencial Teórico, são apresentados os conceitos fundamentais a respeito da análise de imagens em patologia, os casos de uso utilizados neste trabalho, o funcionamento do ambiente de execução Region Templates e suas principais características, os conceitos da otimização multiobjetivo e o funcionamento dos Algoritmos Genéticos clássicos. No Capítulo 3, explica-se a visão geral da solução proposta neste trabalho através do detalhamento do módulo de ajuste automático de parâmetros e do módulo de Análises Comparativas do Region Templates, os algoritmos de otimização suportados e as métricas propostas e implementadas para avaliar a qualidade das segmentações. %O Capítulo 4 explica o módulo de análise comparativa espacial desenvolvido neste trabalho, que é responsável por efetuar o processamento de métricas e consultas espaciais, %além da revisão literária das outras aplicações e ferramentas utilizadas neste trabalho. 
O Capítulo 4 discute os procedimentos experimentais utilizados para o sistema de ajuste automático de parâmetros e compara os resultados dos algoritmos de otimização suportados utilizando as métricas desenvolvidas. No Capítulo 5 são apresentadas as conclusões, as contribuições, e as perspectivas para a continuação do trabalho.
