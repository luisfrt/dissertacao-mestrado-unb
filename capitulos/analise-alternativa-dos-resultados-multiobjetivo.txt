
\paragraph{Peso: Métrica 1 e Tempo 1}

Os resultados deste ajuste multiobjetivo mostram que foi possível melhorar a qualidade e a velocidade da segmentação para ambos os fluxos de trabalho. 

Para o fluxo de trabalho baseado em Operações Morfológicas o algoritmo PRO pôde melhorar a qualidade média do resultado em 1,11$\times$ e também acelerar a velocidade total do processo de segmentação em 1,07$\times$. 

Para o fluxo de trabalho baseado em \emph{Level Set}, nós alcançamos melhorias de qualidade e velocidade para todos os métodos de \emph{declumping}. Para o \emph{Mean Shift Declumping}, o GA e o Spearmint melhoraram a qualidade da métrica em 1,13$\times$ e 1,15$\times$ respectivamente. No entanto, o GA também conseguiu reduzir o tempo de segmentação em 3,99$\times$ enquanto o Spearmint não. O melhor resultado para este fluxo de trabalho e essa combinação de pesos foi do NM que conseguiu reduzir o tempo de execução em 4,91$\times$. No entanto o seu ganho de qualidade no resultado foi menor do que o GA e do Spearmint. % O ganho de velocidade e qualidade foi ainda maior para a estratégia \emph{No Declumping} ao utilizar esses dois algoritmos, alcançando uma aceleração no tempo de execução da aplicação em quase 20 vezes e ainda melhorando a qualidade do resultado em 1,03$\times$. 
O \emph{Watershed Declumping} foi a estratégia em que foi mais possível melhorar o resultado da qualidade da segmentação. O algoritmo GA encontrou conjuntos de parâmetros que aumentaram, em média, a métrica em 1,20$\times$ e também reduziram o tempo de execução para o processo de segmentação em 14,26$\times$. Já o algoritmo NM apresentou um ganho na velocidade de execução ainda maior, o melhor obtido em nossos testes, de 16,05$\times$. No entanto, neste algoritmo houve novamente uma redução no ganho da qualidade do resultado. O algoritmo Spearmint novamente foi o que melhor conseguiu aprimorar a métrica de qualidade, no então esse ganho de qualidade não veio acompanhado da redução no tempo de execução.

Notamos que a abordagem bayesiana (Spearmint) foi muito eficiente para o ajuste mono-objetivo, mas para este esforço multiobjetivo ela não obteve bom desempenho no quesito redução do tempo de execução quando comparado aos outros algoritmos. Também observamos que este algoritmo foi o que mais reduziu o tempo de execução da segmentação para a estratégia \emph{Watershed}, mas não conseguiu manter os resultados de qualidade.

\paragraph{Peso: Métrica 2 e Tempo 1}

A otimização multiobjetivo utilizando estes pesos melhorou o resultado da qualidade geral e diminuiu marginalmente o ganho de aceleração quando comparado com o esforço de peso 1 para a métrica e peso 1 para o tempo. 

Para o fluxo de trabalho baseado em Operações Morfológicas, os melhores algoritmos em relação à qualidade foram os algoritmos de otimização GA, NM e PRO. Em relação à redução do tempo de execução da função de segmentação o GA foi o algoritmo que apresentou o melhor resultado, atingindo uma aceleração de 1,08$\times$ e melhorando marginalmente o resultado da qualidade. 

Para o fluxo de trabalho baseado em \emph{Level Set} também foi possível melhorar a qualidade e a velocidade em todas as estratégias de \emph{declumping}. O GA foi o melhor algoritmo de otimização para este fluxo de trabalho considerando os resultados da métrica de qualidade e a aceleração no processo de segmentação. Os melhores resultados de qualidade foram obtidos com o método de \emph{declumping Watershed}, uma melhoria no resultado geral em 1,26$\times$. Já em relação à redução do tempo de execução, os melhores resultados são do NM também na estratégia de \emph{declumping Watershed}.


\paragraph{Peso: Métrica 4 e Tempo 1}

%Os resultados do par de peso métrica e tempo m4-t1 mostram na Tabela \ref{tab:tuning-single-image} os melhores resultados de ganho de qualidade entre todos os esforços de ajuste multiobjetivo testados.

Todos os quatro algoritmos de otimização obtiveram desempenho semelhante no fluxo de trabalho baseado em Operações Morfológicas para este par de pesos. Todos eles melhoraram a qualidade da segmentação de saída e reduziram o tempo de execução. 

Para o fluxo de trabalho baseado em \emph{Level Set}, os algoritmos de otimização também melhoraram a qualidade da segmentação em todas as estratégias de \emph{declumping}. O GA foi melhor algoritmo de ajuste para este fluxo de trabalho, considerando tanto os resultados da melhoria na qualidade do resultado quanto os de aceleração no processo de segmentação. 

Os melhores resultados de qualidade foram obtidos na estratégia de \emph{Watershed Declumping}, atingindo até 1,28$\times$ de melhoria, valor muito próximo ao encontrado no ajuste mono-objetivo quando se utilizou os mesmos algoritmos de otimização (1,30$\times$). Nestes resultados, além de melhorar a qualidade da segmentação também foi possível melhorar o tempo de execução da segmentação em 11,79$\times$ quando comparado com a segmentação gerada com os parâmetros padrão.

