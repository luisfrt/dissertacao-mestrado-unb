all: clean "Mestrado - Qualificação.tex"
"Mestrado - Qualificação.pdf":
	pdflatex "Mestrado - Qualificação.tex"
	bibtex "Mestrado - Qualificação.aux"
	pdflatex "Mestrado - Qualificação.tex"
	pdflatex "Mestrado - Qualificação.tex"
clean:
	rm -f *.log *.aux *.bbl *.brf *.blg *.dvi *.ps *.lot *.toc *.lof *.out *~ "Mestrado - Qualificação.pdf"
